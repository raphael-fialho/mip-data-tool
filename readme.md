# Mip Data Tool
___

Ferramenta para geração do arquivo _xml_ do mip data.
Exportar xml de mip data. 
Gera um arquivo baseado nos arquivos de entrada, onde as chaves são carregadas do primeiro arquivo, os valores são 
carregados do segundo arquivo.
O arquivo _mip_data_output.xml_ é gerado no diretório _out/_, caso a opção [--to_file] 
seja informada.
Caso exista alguma chave duplicada, uma exceção é lançada.

## Requirements
* Python 3.6


### USAGE

* Man
```
python3 exporter.py -h
```

* Print no console
```
python3 exporter.py keys.xml values.xml
```

* Gerando arquivo (_out/mip_data_output.xml_)
```
python3 exporter.py --to_file keys.xml values.xml
```

* Substituindo valores de chaves novas
```
python3 exporter.py --to_file --miss_value original#replacement keys.xml values.xml
```
