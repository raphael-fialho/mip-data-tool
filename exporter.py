from modules.args_factory import create_parser_for_exporter
from modules.mip_service import load
from modules.mip_service import write
from modules.mip_service import merge
from modules.mip_service import build_miss_data


if __name__ == '__main__':
    parser = create_parser_for_exporter()
    args = parser.parse_args()

    keys_data = load(args.keys_file)
    values_data = load(args.values_file)
    miss_value_data = build_miss_data(args.miss_value)
    not_fail_warn = args.not_fail_warn

    out_data = merge(keys_data, values_data, miss_value_data, not_fail_warn)

    write(out_data, args.to_file)
