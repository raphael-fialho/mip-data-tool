import argparse


def create_parser_for_exporter():
    parser = argparse.ArgumentParser(description='''
    Ferramenta para geracao do arquivo _xml_ do mip data.
    Exportar xml de mip data. 
    Gera um arquivo baseado nos arquivos de entrada, onde as chaves sao carregadas do primeiro, e os valores sao 
    carregados do segundo arquivo.
    O arquivo mip_data_output.xml e gerado no diretorio out/, caso a opcao [--to_file] seja informada.  
    ''')

    parser.add_argument("keys_file", type=str, help=": O arquivo xml onde serao mantidas as chaves.")
    parser.add_argument("values_file", type=str, help=": O arquivo xml onde serao mantidos os valores.")
    parser.add_argument("--miss_value", type=str, help=""""
    : Valor de host padrao. Caso o arquivo de valores nao possuir uma chave presente no arquivo de chaves, esse valor de
    host sera substituido. Se essa opcao nao for configurada, sera mantido o valor original do arquivo de chaves.
    Exemplo: [hostA#hostB]. No exemplo todo o hostA sera substutuido por hostB
    """)
    parser.add_argument("--not_fail_warn", action='store_true', help=""""
        : Suprime o erro quando o arquivo de valores nao possuir uma das chaves.
        """)

    parser.add_argument("--to_file", action='store_true', help=": escreve no arquivo out/mip_data_output.xml")

    return parser
