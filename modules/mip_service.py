import os
import lxml.etree as ET

from copy import deepcopy


def load(xml_file):
    parser = ET.XMLParser(remove_comments=False, encoding="utf-8")
    return ET.parse(xml_file, parser=parser)


def write(data, print_to_file):
    xml = ET.tostring(data, pretty_print=True, xml_declaration=True, encoding="utf-8")

    if print_to_file:
        filename = "out/mip_data_output.xml"

        if not os.path.exists(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename))

        with open(filename, "wb") as f:
            f.write(xml)
    else:
        print(xml.decode())


def build_miss_data(data):
    if data:
        return {
            'orig': data.split('#')[0],
            'repl': data.split('#')[1],
        }
    else:
        return None


def merge(keys_data, values_data, miss_value_data, not_crash_missed_value):
    merged = deepcopy(keys_data)
    __check_duplicate_keys(merged)
    att_values = __get_all_attribute_values(values_data)

    for root in merged.getiterator():
        for macro_el in root.getchildren():
            for child in macro_el.getchildren():
                for att in child.attrib:
                    if 'name' in child.attrib:
                        if child.attrib['name'] in att_values:
                            child.attrib[att] = att_values[child.attrib['name']][att]
                        elif miss_value_data:
                            child.attrib[att] = child.attrib[att].replace(miss_value_data['orig'], miss_value_data['repl'])
                        elif not not_crash_missed_value:
                            raise Exception(
                                'A chave "{}" não foi encontrada no arquivo de valores (arg 2). '
                                'Ative a opcao [--not_fail_warn] para suprimir esse warn e seguir com o valor original '
                                'ou faca o replace pela opcao [--miss_value]'
                                .format(child.attrib['name']))

    return merged


def __get_all_attribute_values(data):

    values_map = {}

    for root in data.getiterator():
        for macro_el in root.getchildren():
            for child in macro_el.getchildren():
                if 'name' in child.attrib:
                    values_map[child.attrib['name']] = child.attrib

    return values_map


def __check_duplicate_keys(data):

    keys_found = []

    for root in data.getiterator():
        for macro_el in root.getchildren():
            for child in macro_el.getchildren():
                if 'name' in child.attrib:
                    if child.attrib['name'] in keys_found and child.tag.endswith('Service'):
                        raise Exception('A chave "{}" está duplicada!!'.format(child.attrib['name']))
                    else:
                        keys_found.append(child.attrib['name'])

    return True
